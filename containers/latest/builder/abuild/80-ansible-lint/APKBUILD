# Contributor: Fabian Affolter <fabian@affolter-engineering.ch>
# Contributor: Artem Korezin <source-email@yandex.ru>
# Maintainer: Artem Korezin <source-email@yandex.ru>
pkgname=ansible-lint
_pkgname=ansible-lint
# renovate: datasource=pypi depName=ansible-lint
pkgver=6.2.1
pkgrel=99
pkgdesc='A tool to check ansible playbooks'
url='https://github.com/ansible/ansible-lint'
arch='noarch'
license='MIT'

# renovate: datasource=repology depName=alpine_3_16/python3 versioning=loose
depends="$depends python3=3.10.4-r0"
# renovate: datasource=repology depName=alpine_3_16/git versioning=loose
depends="$depends git=2.36.1-r0"
# renovate: datasource=repology depName=alpine_3_16/py3-ruamel.yaml versioning=loose
depends="$depends py3-ruamel.yaml=0.16.12-r2"
# renovate: datasource=repology depName=alpine_3_16/py3-rich versioning=loose
depends="$depends py3-rich=12.4.1-r0"
# renovate: datasource=repology depName=alpine_3_16/py3-yaml versioning=loose
depends="$depends py3-yaml=6.0-r0"
# renovate: datasource=repology depName=alpine_3_16/py3-packaging versioning=loose
depends="$depends py3-packaging=21.3-r0"
# renovate: datasource=repology depName=alpine_3_16/py3-wcmatch versioning=loose
depends="$depends py3-wcmatch=8.3-r0"
# renovate: datasource=repology depName=alpine_3_16/py3-enrich versioning=loose
depends="$depends py3-enrich=1.2.7-r0"
# renovate: datasource=repology depName=alpine_3_16/py3-ansible-compat versioning=loose
depends="$depends py3-ansible-compat=2.0.4-r0"
# renovate: datasource=repology depName=alpine_3_16/py3-jsonschema versioning=loose
depends="$depends py3-jsonschema=4.5.1-r0"
# renovate: datasource=pypi depName=ansible-core
depends="$depends ansible-core=2.13.0-r99"
# renovate: datasource=pypi depName=yamllint
depends="$depends yamllint=1.26.3-r99"

# renovate: datasource=repology depName=alpine_3_16/py3-pip versioning=loose
makedepends="$makedepends py3-pip=22.1.1-r0"
# renovate: datasource=repology depName=alpine_3_16/py3-wheel versioning=loose
makedepends="$makedepends py3-wheel=0.37.1-r0"
# renovate: datasource=repology depName=alpine_3_16/py3-setuptools_scm versioning=loose
makedepends="$makedepends py3-setuptools_scm=6.4.2-r1"

source="https://files.pythonhosted.org/packages/source/${_pkgname::1}/$_pkgname/$_pkgname-$pkgver.tar.gz"
options='!check'

package() {
	python3 \
		-m pip \
		install \
		--no-deps \
		--no-compile \
		--no-build-isolation \
		--prefix=/usr \
		--root="$pkgdir" \
		.
}
sha512sums="
2c28eab172722089e98210a826a633573f4c678d254cc5a43ade41eaf309f8e570bdd2cd40528c43bc51a02c72a5fa7b769689da28ed73b2881f689820a835a0  ansible-lint-6.2.1.tar.gz
"
